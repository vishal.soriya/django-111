from django.shortcuts import render

# Create your views here.
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from tutorial.quickstart.serializers import UserSerializer, GroupSerializer


from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from django.http import HttpResponse
from django.views import View
# from google.cloud import bigquery
import json

class BigqueryView(View):
    def get(self, request):
        # client = bigquery.Client()
        # query_job = client.query("""
        #     SELECT date_time as date_time,methane_ppm as methane_ppm FROM `test_dataset_praveen.node_data`  LIMIT 1000""")
        #
        # results = query_job.result()  # Waits for job to complete.
        # dataList = []
        # for row in results:
        #     dataJson = {}
        #     dataJson["date_time"] = row.get('date_time')
        #     dataJson["methane_ppm"] = row.get('methane_ppm')
        #     dataList.append(dataJson)
        dataList = [{"name":"Vishal Soriya", "empCode":2912}]
        return HttpResponse(json.dumps(dataList))


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer